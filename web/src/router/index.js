import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";
import LoginView from "../views/LoginView.vue";
import ForgetPass from "../views/ForgetPass.vue";
//admin
import AdminHomeView from "../views/admin/AdminHomeView.vue";
import AdminTeacherView from "../views/admin/AdminTeacherView.vue";
import AddTeacher from "../views/admin/sub-page/AddTeacher.vue";
import Subject from "../views/admin/Subject.vue";
import AddSubject from "../views/admin/sub-page/AddSubject.vue";
import ChangePass from "../views/admin/ChangePass.vue";
import Logout from "../views/admin/Logout.vue";
//teacher
import TeacherHomeView from "../views/teacher/TeacherHomeView.vue";
import TeacherStudent from "../views/teacher/TeacherStudent.vue";
import TeacherDatabase from "../views/teacher/TeacherDatabase.vue";
import TeacherTest from "../views/teacher/TeacherTest.vue";
import TeacherPass from "../views/teacher/TeacherPass.vue";
import TeacherLogout from "../views/teacher/TeacherLogout.vue";
import AddStudent from "../views/teacher/sub-page/AddStudent.vue";
import AddMultiStudent from "../views/teacher/sub-page/AddMultiStudent.vue";
import ViewQInTest from "../views/teacher/sub-page/ViewQInTest.vue";
import AddTest from "../views/teacher/sub-page/AddTest.vue";
import AddQuestion from "../views/teacher/sub-page/AddQuestion.vue";
import ManualQuestion from "../views/teacher/sub-page/ManualQuestion.vue";
import AutoQuestion from "../views/teacher/sub-page/AutoQuestion.vue";
//student
import StudentHomeView from "../views/student/StudentHomeView.vue";
import StudentTest from "../views/student/StudentTest.vue";
import StudentLogout from "../views/student/StudentLogout.vue";
import TestScreen from "../views/student/sub-page/TestScreen.vue";

const routes = [
  {
    path: "/",
    name: "home",
    component: LoginView,
  },
  //Admin Links
  {
    path: "/admin",
    name: "Admin",
    component: AdminHomeView,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin/teacher",
    name: "manageTeacher",
    component: AdminTeacherView,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin/teacher/add",
    name: "addTeacher",
    component: AddTeacher,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin/subjects",
    name: "subject",
    component: Subject,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin/subjects/add",
    name: "addSubject",
    component: AddSubject,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin/changePass",
    name: "changePass",
    component: ChangePass,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/admin/logout",
    name: "logout",
    component: Logout,
  },
  // Teacher Links
  {
    path: "/teacher",
    name: "teacher",
    component: TeacherHomeView,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/student",
    name: "manageStudent",
    component: TeacherStudent,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/student/add",
    name: "createStudent",
    component: AddStudent,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/student/multiple-add",
    component: AddMultiStudent,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/db",
    name: "database",
    component: TeacherDatabase,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/test",
    name: "manageTest",
    component: TeacherTest,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/test/new",
    name: "newTest",
    component: AddTest,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/test/view",
    name: "viewQuest",
    component: ViewQInTest,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/test/new-question",
    name: "newQuestion",
    component: AddQuestion,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/test/new-question/manual",
    name: "newManual",
    component: ManualQuestion,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/test/new-question/auto",
    name: "newAuto",
    component: AutoQuestion,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/changePass",
    name: "teacherChangePass",
    component: TeacherPass,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/teacher/logout",
    name: "teacherLogout",
    component: TeacherLogout,
  },
  //Student Links
  {
    path: "/student",
    name: "student",
    component: StudentHomeView,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/student/test",
    name: "test",
    component: StudentTest,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/student/test2",
    name: "testScreen",
    component: TestScreen,
    meta: {
      requiresAuth: true,
    },
  },
  {
    path: "/student/logout",
    name: "StudentLogout",
    component: StudentLogout,
  },
  {
    path: "/login",
    name: "login",
    component: LoginView,
  },
  {
    path: "/reset",
    name: "resetPass",
    component: ForgetPass,
  },
  {
    path: "/about",
    name: "about",
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    component: function () {
      return import(/* webpackChunkName: "about" */ "../views/AboutView.vue");
    },
  },
];

const router = createRouter({
  history: createWebHistory(),
  routes,
});

router.beforeEach((to, from, next) => {
  if (to.matched.some((record) => record.meta.requiresAuth)) {
    // this route requires auth, check if logged in
    // if not, redirect to login page.
    console.log("not login");
    if (!sessionStorage.getItem("isLogin")) {
      next({
        path: "/login",
        query: { redirect: to.fullPath },
      });
    } else {
      next();
    }
  } else {
    next(); // make sure to always call next()!
  }
});

export default router;
