import axios from "axios";

class CoreService {
  _headers = {
    "x-api-version": `${process.env.VUE_APP_API_VERSION}`,
  };

  _url = process.env.VUE_APP_HOST_API;

  async login(username, password) {
    let data = {
      username,
      password,
    };
    return await axios.post(`${this._url}/api/user/login`, data, {
      headers: this._headers,
    });
  }

  async getAllTeacher() {
    return await axios.get(`${this._url}/api/teachers`, {
      headers: this._headers,
    });
  }

  async getAllSubject() {
    return await axios.get(`${this._url}/api/subjects`, {
      headers: this._headers,
    });
  }

  async getAllTestGroup() {
    return await axios.get(`${this._url}/api/quiz`, {
      headers: this._headers,
    });
  }
}

export default CoreService;
