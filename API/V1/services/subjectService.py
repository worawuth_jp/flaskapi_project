import json

from V1.queries import userQuery
from V1.repositories.subjectRepository import SubjectRepository
from V1.repositories.teacherRepository import TeacherRepository
from V1.repositories.userRepository import UserRepository
from constants import httpError
from models.ErrorModel import SystemErrorModel
from modules.DBConnector import DBConnector
from modules.logger import Logger

logger = Logger().getLogger("SubjectService")


class SubjectService:

    def getSubject(self):
        try:
            subjectRepo = SubjectRepository()
            result = subjectRepo.findSubject()
            return result
        except Exception as e:
            logger.error("SubjectService getSubject() Exception: " + json.dumps(e.__dict__))
            if not hasattr(e, "statusCode"):
                raise SystemErrorModel(httpError.HTTP_INTERNAL_SERVER_ERROR_STATUS_CODE,
                                       httpError.HTTP_INTERNAL_SERVER_ERROR_MSG)
            raise SystemErrorModel(e.statusCode, e.message, e.stack)