from flask import Response, jsonify

from constants import httpError, http
from models.ResponseBasic import ResponseBasic
from routers.courseRouter import course_api
from routers.databaseRouter import database_api
from routers.majorRouter import major_api
from routers.quizRouter import quiz_api
from routers.studentRouter import student_api
from routers.subjectRouter import subject_api
from routers.teacherRouter import teacher_api
from routers.testRouter import test_api
from routers.adminRouter import admin_api
from routers.userRouter import user_api
from routers.prenameRouter import prename_api


def router(app):
    @app.errorhandler(404)
    def handle_page_not_found(e):
        result = ResponseBasic(httpError.HTTP_PAGE_NOT_FOUND_CODE,httpError.HTTP_PAGE_NOT_FOUND_MSG)
        #result = {"statusCode": httpError.HTTP_PAGE_NOT_FOUND_CODE, "message": httpError.HTTP_PAGE_NOT_FOUND_MSG}
        return jsonify(result.__dict__), httpError.HTTP_PAGE_NOT_FOUND_CODE

    @app.route("/hello",methods=['GET'])
    def hello():
        result = ResponseBasic(http.HTTP_SUCCESS_CODE,"HELLO API")
        return jsonify(result.__dict__), http.HTTP_SUCCESS_CODE

    app.register_error_handler(404, handle_page_not_found)
    app.register_blueprint(test_api, url_prefix='/api')
    app.register_blueprint(admin_api, url_prefix='/api')
    app.register_blueprint(user_api, url_prefix='/api')
    app.register_blueprint(teacher_api, url_prefix='/api')
    app.register_blueprint(subject_api, url_prefix='/api')
    app.register_blueprint(student_api, url_prefix='/api')
    app.register_blueprint(prename_api, url_prefix='/api')
    app.register_blueprint(major_api, url_prefix='/api')
    app.register_blueprint(course_api, url_prefix='/api')
    app.register_blueprint(database_api, url_prefix='/api')
    app.register_blueprint(quiz_api, url_prefix='/api')

