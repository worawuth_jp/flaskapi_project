from constants import dataTable

FIND_ALL_SUBJECT = "SELECT * FROM {table} s INNER JOIN {courses} c ON s.course_id = c.course_id".format(table=dataTable.SUBJECTS_TABLE, courses=dataTable.COURSES_TABLE)