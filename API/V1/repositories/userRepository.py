import json

from V1.queries import userQuery
from constants import httpError
from models.ErrorModel import SystemErrorModel
from modules.DBConnector import DBConnector
from modules.logger import Logger
import numpy as np

logger = Logger().getLogger("UserRepository")


class UserRepository:
    dbConnector = DBConnector("test")

    def getUsers(self):
        try:
            self.con = self.dbConnector.connect()
            self.cursor = self.dbConnector.getCursor()
            self.cursor.execute(userQuery.FIND_USERS)

            return self.cursor.fetchall()
        except Exception as e:
            logger.error("UserRepository getUsers() Exception : " + json.dumps(e.__dict__))
            if not hasattr(e, 'statusCode'):
                raise SystemErrorModel(httpError.HTTP_INTERNAL_SERVER_ERROR_STATUS_CODE,
                                       httpError.HTTP_INTERNAL_SERVER_ERROR_MSG, str(e))
            raise SystemErrorModel(e.statusCode, e.message, e.stack)
        finally:
            self.con.commit()
            self.cursor.close()
            self.con.close()

    def userLogin(self, username, password):
        try:
            self.con = self.dbConnector.connect()
            self.cursor = self.dbConnector.getCursor()
            logger.info("QUERY : " + userQuery.USER_LOGIN(username, password))
            self.cursor.execute(userQuery.USER_LOGIN(username, password))
            result = {}
            item = self.cursor.fetchone()
            result['isUserExist'] = False
            result['isLoginSuccess'] = False
            if item :
                result['isUserExist'] = item['USER_EXIST'] == 1
                result['isLoginSuccess'] = item['USER_LOGIN'] == 1
                result['userData'] = {}
                result['userData']['username'] = item['user_username']
                result['userData']['userId'] = item['user_id']
                result['userData']['userType'] = item['user_type']
                result['userData']['firstname'] = item['user_firstname']
                result['userData']['lastname'] = item['user_lastname']
                result['userData']['email'] = item['user_email']
                result['userData']['prenameId'] = item['user_prename']
                result['userData']['prenameText'] = item['prename_text']

            return result
        except Exception as e:
            logger.error("UserRepository userLogin Exception: " + json.dumps(e.__dict__))
            if not hasattr(e, 'statusCode'):
                raise SystemErrorModel(httpError.HTTP_INTERNAL_SERVER_ERROR_STATUS_CODE,
                                       httpError.HTTP_INTERNAL_SERVER_ERROR_MSG, str(e))
            raise SystemErrorModel(e.statusCode, e.message, e.stack)
        finally:
            self.con.commit()
            self.cursor.close()
            self.con.close()


    def createUser(self, type, username, password, prename, firstname, lastname ):
        try:
            self.con = self.dbConnector.connect()
            self.cursor = self.dbConnector.getCursor()

            self.cursor.execute(userQuery.INSERT_USER,(username,password,prename,firstname,lastname,type))
            result = {}
            result['userId'] = self.cursor.lastrowid
            result['username'] = username
            return result

        except Exception as e:
            self.con.rollback()
            logger.error("UserRepository createUser Exception: " + json.dumps(e.__dict__))
            self.con.rollback()
            if not hasattr(e, 'statusCode'):
                raise SystemErrorModel(httpError.HTTP_INTERNAL_SERVER_ERROR_STATUS_CODE,
                                       httpError.HTTP_INTERNAL_SERVER_ERROR_MSG, str(e))
            raise SystemErrorModel(e.statusCode, e.message, e.stack)

        finally:
            self.con.commit()
            self.cursor.close()
            self.con.close()

    def editUser(self, type, prename, firstname, lastname, userId ):
        try:
            self.con = self.dbConnector.connect()
            self.cursor = self.dbConnector.getCursor()

            self.cursor.execute(userQuery.EDIT_USER,(prename,firstname,lastname,type, userId))
            result = {}
            result['userId'] = userId
            return result

        except Exception as e:
            self.con.rollback()
            logger.error("UserRepository editUser Exception: " + json.dumps(e.__dict__))
            self.con.rollback()
            if not hasattr(e, 'statusCode'):
                raise SystemErrorModel(httpError.HTTP_INTERNAL_SERVER_ERROR_STATUS_CODE,
                                       httpError.HTTP_INTERNAL_SERVER_ERROR_MSG, str(e))
            raise SystemErrorModel(e.statusCode, e.message, e.stack)

        finally:
            self.con.commit()
            self.cursor.close()
            self.con.close()

    def deleteUser(self,userId ):
        try:
            self.con = self.dbConnector.connect()
            self.cursor = self.dbConnector.getCursor()

            self.cursor.execute(userQuery.DELETE_USER,(userId,))
            result = {}
            result['userId'] = userId
            return result

        except Exception as e:
            self.con.rollback()
            logger.error("UserRepository editUser Exception: " + json.dumps(e.__dict__))
            self.con.rollback()
            if not hasattr(e, 'statusCode'):
                raise SystemErrorModel(httpError.HTTP_INTERNAL_SERVER_ERROR_STATUS_CODE,
                                       httpError.HTTP_INTERNAL_SERVER_ERROR_MSG, str(e))
            raise SystemErrorModel(e.statusCode, e.message, e.stack)

        finally:
            self.con.commit()
            self.cursor.close()
            self.con.close()

    def testQuery(self):
        try:
            self.con = self.dbConnector.connect()
            self.cursor = self.dbConnector.getCursor()
            params = ()
            self.cursor.execute("INSERT INTO test.test_table (id) VALUES(2)")
            self.cursor.execute("SELECT * FROM test.test_table")
            testResult = self.cursor.fetchall()
            self.con.rollback()

            self.cursor.execute("INSERT INTO test.test_table (id) VALUES(2)")
            self.cursor.execute("SELECT * FROM test.test_table")
            expectResult = self.cursor.fetchall()

            return {'test': testResult, 'expect': expectResult, 'compare': np.array_equal(testResult,expectResult)}
        except Exception as e:
            logger.error("TEST testQuery() Exception : " + json.dumps(e.__dict__))
            if not hasattr(e, 'statusCode'):
                raise SystemErrorModel(httpError.HTTP_INTERNAL_SERVER_ERROR_STATUS_CODE,
                                       httpError.HTTP_INTERNAL_SERVER_ERROR_MSG, str(e))
            raise SystemErrorModel(e.statusCode, e.message, e.stack)
        finally:
            # self.con.commit()
            self.cursor.close()
            self.con.close()
